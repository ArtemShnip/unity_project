﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = System.Random;

public class StickHeroController : MonoBehaviour
{
    [SerializeField] private StickHeroStick m_Stick;
    [SerializeField] private StickHeroPlayer m_Player;
    [SerializeField] private StickHeroPlatform[] m_Platforms;

    private int counter;//это счетчик платформ
    
    public enum EGameState
    {
        Wait,
        Scaling,
        Rotate,
        Movement,
        Defeat,
    }

    private EGameState currentGameState;
    
    // Start is called before the first frame update
    private void Start()
    {
        currentGameState = EGameState.Wait;
        counter = 0;
        
        m_Stick.ResetStick(m_Platforms[0].GetStickPosition());
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetMouseButtonDown(0) == false)
        {
            return;
        }

        switch (currentGameState)
        {
            //если не осуществлен старт игры
            case EGameState.Wait:
                currentGameState = EGameState.Scaling;
                m_Stick.StartScaling();
                break;
            // стик увеличивается - прерываем увеличение и запускаем поворот
            case EGameState.Scaling:
                currentGameState = EGameState.Rotate;
                m_Stick.StopScaling();
                GetNewPlatform();
                break;
            
            //ничего не делаем
            case EGameState.Rotate:
                break;
            
            //ничего не делаем
            case EGameState.Movement:
                break;
            
            case EGameState.Defeat:
                print("Defeat");
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                break;
            
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void GetNewPlatform()
    {
        StickHeroPlatform newPlatform = Instantiate(m_Platforms[1]);
        Vector3 pos = m_Player.transform.position;
        Random rnd=new Random();
        double value = rnd.NextDouble();
        pos.x = pos.x + ((float)value + 5f);
        pos.y = pos.y - 0.6f;
        newPlatform.transform.position = pos;
    }

    public float GetMaxSizeStick()
    {
        StickHeroPlatform nextPlatform = m_Platforms[counter + 1];
        float maxSize = 
            nextPlatform.transform.position.x + 1f;
        
        return maxSize;
    }

    public void StopStickScale()
    {
        currentGameState = EGameState.Rotate;
        m_Stick.StartRotate();
    }

    public void StopStickRotate()
    {
        currentGameState = EGameState.Movement;
    }

    public void StartPlayerMovement(float length)
    {
        currentGameState = EGameState.Movement;
        
        StickHeroPlatform nextPlatform = m_Platforms[counter + 1];
        
        //находим минимальную длину стика для успешного перехода
        float targetLength = 
            nextPlatform.transform.position.x - m_Stick.transform.position.x;

        float platformSize = nextPlatform.GetPlatformSize();

        float min = targetLength - platformSize * 0.5f;
        min -= m_Player.transform.localScale.x * 0.9f;
        
        //находим максимальную длину стика для успешного перехода
        float max = targetLength + platformSize * 0.5f;
        
        //при успехе переъодим в центр следующей платформы, иначе падаем

        if (length < min || length > max)
        {
            //будем падать
            float targetPosition = m_Stick.transform.position.x + length +
                                   m_Player.transform.localScale.x;
            
            m_Player.StartMovement(targetPosition, true);
        }
        else
        {
            float targetPosition = nextPlatform.transform.position.x;
            m_Player.StartMovement(targetPosition, false);
        }

    }

    public void StopPlayerMovement()
    {
        currentGameState = EGameState.Wait;
        counter++;
        
        m_Stick.ResetStick(m_Platforms[counter].GetStickPosition());
    }

    public void ShowScores()
    {
        currentGameState = EGameState.Defeat;
        
        print($"Game over at {counter}");
    }
}
