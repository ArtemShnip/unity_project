﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FingerDriverTrack : MonoBehaviour
{

    private class TrackSegment
    {
        public Vector3[] Points;

        public bool IsPointInSegment(Vector3 point)
        {
            return MathfTriangles.IsPointInTriangleXY(
                point, Points[0], Points[1], Points[2]);
        }
    }

    [SerializeField] private LineRenderer m_lineRender;
    [SerializeField] private bool m_debug;
    [SerializeField] private GameObject m_Car;

    private Vector3[] corners;
    private TrackSegment[] segments;
    
    
    // Start is called before the first frame update
    void Start()
    {
        //заполняем массив опорных точек
        corners = new Vector3[transform.childCount];
        for (int i = 0; i < corners.Length; i++)
        {
            GameObject obj = transform.GetChild(i).gameObject;
            corners[i] = obj.transform.position;
            obj.GetComponent<MeshRenderer>().enabled = false;
        }
        
        //настраиваем Line Render
        m_lineRender.positionCount = corners.Length;
        m_lineRender.SetPositions(corners);
        
        //запекаем меш из Line Render
        Mesh mesh = new Mesh();
        m_lineRender.BakeMesh(mesh,true);
        
        //создаем массив сегментов трассы 
        segments = new TrackSegment[mesh.triangles.Length / 3];
        int segmentCounter = 0;

        for (int i = 0; i < mesh.triangles.Length; i+=3)
        {
            segments[segmentCounter] = new TrackSegment();
            segments[segmentCounter].Points = new Vector3[3];
            
            segments[segmentCounter].Points[0] = mesh.vertices[mesh.triangles[i]];
            segments[segmentCounter].Points[1] = mesh.vertices[mesh.triangles[i + 1]];
            segments[segmentCounter].Points[2] = mesh.vertices[mesh.triangles[i + 2]];

            segmentCounter++;
        }


       
        
        //отдельно можно продебажить сегменты
        if (!m_debug)
        {
            return;
        }

        foreach (var segment in segments)
        {
            foreach (var point in segment.Points)
            {
                GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                sphere.transform.position = point;
                sphere.transform.localScale=Vector3.one * 0.1f;
            }
        }
    }

    /// <summary>
    /// определяем находится ли точка на трассе
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    public bool IsPointInTrack(Vector3 point)
    {
        foreach (var segment in segments)
        {
            if (segment.IsPointInSegment(point))
            {
                return true;
            }   
        }

        return false;
    }

    

    public int counterPoint = 0;
    // Update is called once per frame
    void Update()
    {
        if (segments[counterPoint].IsPointInSegment(m_Car.transform.position))
        {
            print($"counterPoint {counterPoint}");
            counterPoint++;
        }
    }
}
