﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FingerDriverInput : MonoBehaviour
{
    [SerializeField] private Transform m_SteerWheelTransform;
    [SerializeField] [Range(0, 180f)] private float m_MaxSteerAngle = 90;
    [SerializeField] [Range(0, 1f)] private float m_SteerAcceleration = 0.25f;

    private float steerAxis;

    public float SteerAxis
    {
        get => steerAxis;
        set => steerAxis = Mathf.Lerp(steerAxis,value,m_SteerAcceleration);
    }

    private Vector2 startSteerWheelPoint;
    private Camera mainCamera;

    // Start is called before the first frame update
    private void Start()
    {
        mainCamera = Camera.main;
        startSteerWheelPoint = mainCamera.WorldToScreenPoint(m_SteerWheelTransform.position);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            //угол между рулем и точкой касания экрана
            float angle = Vector2.Angle(Vector2.up, (Vector2) Input.mousePosition - startSteerWheelPoint);

            angle /= m_MaxSteerAngle;
            angle = Mathf.Clamp01(angle);

            if (Input.mousePosition.x > startSteerWheelPoint.x)
            {
                angle *= -1f;
            }

            steerAxis = angle;
        }
        else
        {
            steerAxis = 0;
        }

        m_SteerWheelTransform.localEulerAngles = new Vector3(0f, 0f, steerAxis * m_MaxSteerAngle);
    }
}
