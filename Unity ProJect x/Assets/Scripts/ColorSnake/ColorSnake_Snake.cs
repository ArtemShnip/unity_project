﻿using System;
using System.Collections;
using System.Collections.Generic;
using ColorSnake;
using UnityEngine;

public class ColorSnake_Snake : MonoBehaviour
{
    [SerializeField] private ColorSnake_GameController m_GameController;
    [SerializeField] private SpriteRenderer m_SpriteRender;

    private int currentType;
    private Vector3 position;
    
    // Start is called before the first frame update
    void Start()
    {
        position = transform.position;
        
        var colorType = m_GameController.Types.GetRandomColorType();
        currentType = colorType.Id;
        m_SpriteRender.color = colorType.Color;
    }

    // Update is called once per frame
    void Update()
    {
        position = transform.position;
        
        if (!Input.GetMouseButton(0))
        {
            return;
        }

        position.x = m_GameController.Camera.ScreenToWorldPoint(Input.mousePosition).x;
        position.x = Mathf.Clamp(position.x, m_GameController.Bounds.Left, m_GameController.Bounds.Right);
        transform.position = position;
    }

    private void SetupColor(int id)
    {
        var colorType = m_GameController.Types.GetColorType(id);
        currentType = colorType.Id;
        m_SpriteRender.color = colorType.Color;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var obstacle = other.gameObject.GetComponent<ColorSnake_Obstacle>();
        if (obstacle == null)
        {
            return;
        }
        
        SetupColor(obstacle.ColorId);
        Destroy(obstacle.gameObject);
    }
}
