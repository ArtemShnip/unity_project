using System;
using System.Collections;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game_2D
{
    public class SceneLoader : MonoBehaviour
    {
        private static string nextLevel;
        public Slider Slider;
        public GameObject player;

        private static void LoadLevel(string level)
        {
            nextLevel = level;
            SceneManager.LoadScene("Loading Scene");
        }

        private IEnumerator Start()
        {
           GameManager.SetGameState(GameState.Loading);
           
           AsyncOperation loadingGame = SceneManager.LoadSceneAsync("Game 2D");
           loadingGame.allowSceneActivation = false;
           
           float sliderValue = 0.1f;
           Vector3 pos = player.transform.position;
           
           while (sliderValue <= 1f)
           {
               sliderValue += 0.01f;
               Slider.value = sliderValue;
               pos.x += 0.095f;
               player.transform.position = pos;
               yield return new WaitForSeconds(0.05f);
           }
           
           while (pos.y > -6f)
           {
               pos.y -= 0.1f;
               player.transform.position = pos;
               if (pos.y < -5.5f && !loadingGame.isDone)
               {
                   loadingGame.allowSceneActivation = true;
               }
               yield return new WaitForSeconds(0.01f);
           }
           
           
           
           if (string.IsNullOrEmpty(nextLevel))
           {
               SceneManager.LoadSceneAsync("Game 2D");
               yield break;
           }
           
           AsyncOperation loading = null;
           loading = SceneManager.LoadSceneAsync(nextLevel, LoadSceneMode.Additive);

           
           
           
           while (loading.isDone)
           {
               yield return null;
           }

           nextLevel = null;
           SceneManager.UnloadSceneAsync("Loading Scene");
        }
    }
}