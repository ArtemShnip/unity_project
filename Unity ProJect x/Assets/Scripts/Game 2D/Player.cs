using System;
using UnityEngine;

namespace Game_2D
{
    public class Player : MonoBehaviour,IPlayer,IHitBox
    {
        [SerializeField] private int health = 1;
        public void RegisterPlayer()
        {
            GameManager manager = FindObjectOfType<GameManager>();

            if (manager.Player == null)
            {
                manager.Player = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Awake()
        {
            RegisterPlayer();
        }

        public int Healt
        {
            get =>health;
            private set
            {
                health = value;
                if (health <= 0f)
                {
                    Die();
                }
            }
        }
        public void Hit(int damage)
        {
            Healt -= damage;
        }

        public void Die()
        {
            print("Player is died");
        }
    }
}