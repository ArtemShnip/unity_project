using System;
using UnityEngine;

namespace Game_2D
{
    public class Enemy : MonoBehaviour,IEnemy,IHitBox
    {
        [SerializeField] private int health = 1;
        public void RegisterEnemy()
        {
            GameManager manager = FindObjectOfType<GameManager>();
            manager.Enemies.Add(this);
        }

        private void Awake()
        {
            RegisterEnemy();
        }

        public int Healt
        {
            get =>health;
            private set
            {
                health = value;
                if (health <= 0f)
                {
                    Die();
                }
            }
        }
        public void Hit(int damage)
        {
            Healt -= damage;
        }

        public void Die()
        {
            print("Player is died");
        }
    }
}