using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace Game_2D
{
    public class test : MonoBehaviour
    {
         private static string nextLevel;
        
                private static void LoadLevel(string level)
                {
                    nextLevel = level;
                    SceneManager.LoadScene("Loading Scene");
                }
        
                private IEnumerator Start()
                {
                   GameManager.SetGameState(GameState.Loading);
                   
                   yield return new WaitForSeconds(1f);
        
                   if (string.IsNullOrEmpty(nextLevel))
                   {
                       SceneManager.LoadScene("Ui Button");
                       yield break;
                   }
        
                   AsyncOperation loading = null;
                   loading = SceneManager.LoadSceneAsync(nextLevel, LoadSceneMode.Additive);
        
                   while (loading.isDone)
                   {
                       yield return null;
                   }
        
                   nextLevel = null;
                   SceneManager.UnloadSceneAsync("Loading Scene");
                }
    }
}