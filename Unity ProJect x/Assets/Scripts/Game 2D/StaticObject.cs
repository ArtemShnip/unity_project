using System;
using UnityEngine;

namespace Game_2D
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(BoxCollider2D))]
    public class StaticObject : MonoBehaviour
    {
        [SerializeField] private LevelObjectData objectData;
        private int health = 1;
        private Rigidbody2D rigidbody;

        private void Start()
        {
            health = objectData.Health;
            rigidbody = GetComponent<Rigidbody2D>();
            rigidbody.bodyType = objectData.isStatic ? RigidbodyType2D.Static : RigidbodyType2D.Dynamic;
        }

#if UNITY_EDITOR
        [ContextMenu("Rename this")]
        private void Rename()
        {
            if (objectData !=null)
            {
                gameObject.name = objectData.name;
            }
        }

        [ContextMenu("Move Right this")]
        private void MoveRight()
        {
            Move(Vector2.right);
        }
        
        [ContextMenu("Move Left this")]
        private void MoveLeft()
        {
            Move(Vector2.left);
        }

        [ContextMenu("Move and Copy Up this")]
        private void MoveUp()
        {
            Instantiate(this);
            Move(Vector2.up);
        }
        
        
        private void Move(Vector2 direction)
        {
            var collider = GetComponent<Collider2D>();
            var boundSize = collider.bounds.size;
            transform.Translate(direction * boundSize);
        }
#endif
        
        public int Healt
        {
            get =>health;
            private set
            {
                health = value;
                if (health <= 0f)
                {
                    Die();
                }
            }
        }
        public void Hit(int damage)
        {
            Healt -= damage;
        }

        public void Die()
        {
            print("Object is died");
        }
    }
}