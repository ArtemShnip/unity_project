    public interface IPlayer
    {
        void RegisterPlayer();
    }

    public interface IEnemy
    {
        void RegisterEnemy();
    }

    public interface IDammage
    {
        int Damage { get; }
        void SetDamage();
    }

    public interface IHitBox
    {
        int Healt { get; }
        void Hit(int damage);
        void Die();
    }