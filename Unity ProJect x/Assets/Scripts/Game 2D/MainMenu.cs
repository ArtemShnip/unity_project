using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game_2D
{
    public class MainMenu : MonoBehaviour
    {
        private void Start()
        {
            GameManager.SetGameState(GameState.MainMenu);
        }

        public void LoadLevel(string level)
        {
            SceneManager.LoadScene(level);
        }
    }
}