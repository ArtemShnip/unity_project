using System;
using UnityEngine;

namespace Hop
{
    public class RunnerInput : MonoBehaviour
    {
        private float strafe;

        public float Strafe => strafe;

        private float ScreenCenter;

        private void Start()
        {
            ScreenCenter = Screen.width * 0.5f;
        }

        private void Update()
        {
            if (!Input.GetMouseButton(0))
            {
                return;
            }

            float mousePos = Input.mousePosition.x;

            if (mousePos > ScreenCenter)
            {
                strafe = (mousePos - ScreenCenter) / ScreenCenter;
            }
            else
            {
                strafe = 1 - mousePos / ScreenCenter;
                strafe *= -1f;
            }
        }
    }
}