﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunnerGate : MonoBehaviour
{
    [SerializeField] private GameObject m_BasePlatform;
    [SerializeField] private GameObject m_DonePlatform;

    private void Start()
    {
        m_DonePlatform.SetActive(false);
        m_BasePlatform.SetActive(true);
    }

    public void SetupDone()
    {
        m_BasePlatform.SetActive(false);
        m_DonePlatform.SetActive(true);
    }
}
