using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Hop
{
    public class RunnerPlayer : MonoBehaviour
    {

        [SerializeField] private AnimationCurve m_JumpCurve;
        [SerializeField] private float m_JumpHeight = 1f;
        [SerializeField] private float m_JumpDistance = 2f;

        [SerializeField] private float m_BallSpeed = 1f;
        [SerializeField] private RunnerInput m_Input;
        [SerializeField] private RunnerTrack m_Track;

        private float interation;//цикл прыжка
        private float startZ;//точка начала прыжка

        private void Update()
        {
            Vector3 pos = transform.position;
            //смещение
            pos.x = Mathf.Lerp(pos.x, m_Input.Strafe, Time.deltaTime * 5f);
            
            //прыжок
            pos.y = m_JumpCurve.Evaluate(interation * m_JumpHeight);
            
            //движение вперед
            pos.z = startZ + interation * m_JumpDistance;

            transform.position = pos;
            
            //увеличиваем счетчик прыжка
            interation += Time.deltaTime * m_BallSpeed;

            if (interation < 1f)
            {
                return;
            }

            interation = 0f;
            startZ += m_JumpDistance;
            
            if (m_Track.IsBallOnPlatform(transform.position))
            {
                return;
            }

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}




















