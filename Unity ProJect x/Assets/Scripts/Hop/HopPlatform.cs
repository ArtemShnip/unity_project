using UnityEngine;

namespace Hop
{
    public class HopPlatform : MonoBehaviour
    {
        [SerializeField] private GameObject m_BasePlatform;
        [SerializeField] private GameObject m_DonePlatform;
        [SerializeField] private GameObject m_RedPlatform;
        
        private void Start()
        {
            m_DonePlatform.SetActive(false);
            m_RedPlatform.SetActive(false);
            m_BasePlatform.SetActive(true);
        }
        
        public void SetupDone()
        {
            m_BasePlatform.SetActive(false);
            m_RedPlatform.SetActive(false);
            m_DonePlatform.SetActive(true);
        }
        
        public void SetupRed()
        {
            m_BasePlatform.SetActive(false);
            m_DonePlatform.SetActive(false);
            m_RedPlatform.SetActive(true);
        }
    }
}