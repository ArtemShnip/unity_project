using System.Collections.Generic;
using UnityEngine;

namespace Hop
{
    public class HopTrack : MonoBehaviour
    {
        [SerializeField] private GameObject m_Platform;
        private List<GameObject> platforms = new List<GameObject>();
        private HopPlayer m_hopPlayer;
            
            
            
            // Start is called before the first frame update
        void Start()
        { 
            platforms.Add(m_Platform.gameObject); 
            for (int i = 0; i < 25; i++)
            {
                GameObject obj = Instantiate(m_Platform.gameObject, transform);
                Vector3 pos = Vector3.zero;
                pos.z = 2 * (i + 1);
                pos.x = Random.Range(-1, 2);
                obj.transform.position = pos;
                obj.name = $"Platform {i}";
                platforms.Add(obj);
            }
        }

        private bool speedUp = false;
        private bool jumpUp = false;
        public bool IsBallOnPlatform(Vector3 position)
        {
            position.y = 0f;
            GameObject nearestPlatform = platforms[0];
            for (int i = 0; i < platforms.Count; i++)
            {
                if (platforms[i].transform.position.z + 0.5f < position.z)
                {
                    continue;
                }
                    
                if (platforms[i].transform.position.z - position.z > 1f)
                {
                    continue;
                }
        
                nearestPlatform = platforms[i];
                break;
            }
        
            float minX = nearestPlatform.transform.position.x - 0.5f;
            float maxX = nearestPlatform.transform.position.x + 0.5f;
        
            bool isDone = position.x > minX && position.x < maxX;
        
            
            if (isDone)
            { 
                HopPlatform gate = nearestPlatform.GetComponent<HopPlatform>();
                gate.SetupDone();
                
                if (platforms[4].transform.position == nearestPlatform.transform.position)
                {
                    gate.SetupRed();
                    speedUp = true;
                }
                if (platforms[2].transform.position == nearestPlatform.transform.position)
                {
                    gate.SetupRed();
                    jumpUp = true;
                }
            }
                
            return isDone;
        }

        public bool Speed()
        {
            if (speedUp)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        public bool Jump()
        {
            if (jumpUp)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}