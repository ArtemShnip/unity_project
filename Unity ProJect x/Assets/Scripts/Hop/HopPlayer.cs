using UnityEngine;
using UnityEngine.SceneManagement;

namespace Hop
{
    public class HopPlayer : MonoBehaviour
    {
        [SerializeField] private AnimationCurve m_JumpCurve;
        [SerializeField] private float m_JumpHeight = 1f;
        [SerializeField] private float m_JumpDistance = 2f;

        [SerializeField] private float m_BallSpeed = 1f;
        [SerializeField] private HopInput m_Input;
        [SerializeField] private HopTrack m_Track;

        private float interation;//цикл прыжка
        private float startZ;//точка начала прыжка

        private void Update()
        {
            Vector3 pos = transform.position;
            //смещение
            pos.x = Mathf.Lerp(pos.x, m_Input.Strafe, Time.deltaTime * 5f);
            
            //прыжок
            pos.y = m_JumpCurve.Evaluate(interation * m_JumpHeight);
            
            //движение вперед
            pos.z = startZ + interation * m_JumpDistance;

            transform.position = pos;
            
            //увеличиваем счетчик прыжка
            interation += Time.deltaTime * m_BallSpeed;

            if (interation < 1f)
            {
                return;
            }

            interation = 0f;
            startZ += m_JumpDistance;
            
            if (m_Track.IsBallOnPlatform(transform.position))
            {
                if (m_Track.Speed())
                {
                    //увелечение скорости
                    m_BallSpeed = 1.5f; 
                    
                }

                if (m_Track.Jump())
                {
                    //увелечение высоты прыжка и дистанции
                    m_JumpCurve.RemoveKey(1);
                    m_JumpCurve.AddKey(0.5f, 1f);
                    m_JumpDistance = 4f;
                }
                return;
            }

            

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}